from netmiko import ConnectHandler
from getpass import getpass

cisco_router = list()
print(device)
k=1
ACK='y'
while ACK='y':
    device_IP = input("Nhập địa chỉ IP: ")
    device_username = input("Nhập username: ")
    device_pass=getpass('Nhập mật khẩu: ')
    cisco_router[k] = {
        'device_type': 'cisco_ios',
        'host': device_IP,
        'username': device_username,
        'passwordSSH': device_pass,
    }
    net_connect=ConnectHandler(**cisco_router)
    net_connect.send_config_set(['int f0/1', 'ip add 192.168.10.1 255.255.255.0', 'no shutdown', 'exit'])
    net_connect.send_config_set(['int loopback 0', 'ip add 192.168.20.1 255.255.255.0', 'no shutdown', 'exit'])
    output = net_connect.send_command('show ip int br')
    with open('router_ip_int.txt', 'w+') as file_access:
        file_access.write("IP interfaces:")
        file_access.write(output)
    ACK=input('Nhấn y để tiếp tục: ')

n=len(cisco_router)
print(f"Hiện tại có %n thiết bị trong mạng")